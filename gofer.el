;;; gofer.el --- browsing gopher sites.
;;
;; Copyright © David Edmondson
;;
;; gofer.el is free software: you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; gofer.el is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with gofer.el.  If not, see <https://www.gnu.org/licenses/>.
;;
;; Authors: David Edmondson <dme@dme.org>

;;; Code:

(eval-when-compile (require 'cl-macs))

;;

(defvar gofer--buffer-name "*gofer*"
  "The name of the gofer buffer.")

(defvar gofer--current-address nil
  "The gopher address of the current buffer.")
(make-local-variable 'gofer--current-address)

(defvar gofer--current-process nil
  "The network process of the current buffer.")
(make-local-variable 'gofer--current-process)

(defvar gofer--pending-input nil
  "The pending input of the current gopher request.")
(make-local-variable 'gofer--pending-input)

(defvar gofer--target-place nil
  "The desired value of point.")
(make-local-variable 'gofer--target-place)

;;

(defstruct gofer-address
  host port type selector)

(defstruct gofer-handler
  type name coding
  search saver
  filter line-filter
  directory-processor accumulator-processor)

;;

(defvar gofer--handlers
  (list (cons "0" (make-gofer-handler :type "0"
				      :name "file"
				      :filter 'gofer--line-filter
				      :line-filter 'gofer--file-filter
				      :directory-processor 'gofer--directory-link))
	(cons "1" (make-gofer-handler :type "1"
				      :name "dir"
				      :filter 'gofer--line-filter
				      :line-filter 'gofer--directory-filter
				      :directory-processor 'gofer--directory-link))
	(cons "7" (make-gofer-handler :type "7"
				      :name "search"
				      :search t
				      :filter 'gofer--line-filter
				      :line-filter 'gofer--directory-filter
				      :directory-processor 'gofer--directory-link))
	(cons "i" (make-gofer-handler :type "i"
				      :name "info"
				      :directory-processor 'gofer--directory-info))
	(cons "I" (make-gofer-handler :type "I"
				      :name "image"
				      :coding 'binary
				      :saver 'gofer--accumulator-saver
				      :filter 'gofer--accumulator-filter
				      :accumulator-processor 'gofer--accumulator-image
				      :directory-processor 'gofer--directory-link))
	(cons "p" (make-gofer-handler :type "p"
				      :name "png"
				      :coding 'binary
				      :saver 'gofer--accumulator-saver
				      :filter 'gofer--accumulator-filter
				      :accumulator-processor 'gofer--accumulator-image
				      :directory-processor 'gofer--directory-link))
	(cons "g" (make-gofer-handler :type "g"
				      :name "gif"
				      :coding 'binary
				      :saver 'gofer--accumulator-saver
				      :filter 'gofer--accumulator-filter
				      :accumulator-processor 'gofer--accumulator-image
				      :directory-processor 'gofer--directory-link))
	(cons "h" (make-gofer-handler :type "h"
				      :name "url"
				      :directory-processor 'gofer--directory-url))
	(cons "." (make-gofer-handler :type "."
				      :directory-processor 'gofer--directory-terminal))
	)
  "Handlers for different types of gopher documents.")

;;

(defun gofer--leader (name)
  "Insert an appropriate leader for the current line named NAME."
  (let ((colon (if (string= name "") " " ":")))
    (format "%6s%s " name colon)))

(defun gofer--directory-link-1 (type display selector property-name property-value
				     &optional host port)
  "Insert a link directory entry."
  (let ((host (or host
		  (gofer-address-host gofer--current-address)))
	(port (or port
		  (gofer-address-port gofer--current-address)))
	(h (gofer--get-handler type)))
    (insert (gofer--leader (gofer-handler-name h))
	    (propertize display
			'face 'link
			property-name property-value)
	    "\n")))

(defun gofer--directory-link (type display selector &optional host port)
  "Insert a link directory entry of TYPE (file, directory, image, ...)."
  (gofer--directory-link-1 type display selector
			   'gofer-target (gofer--format-address
					  (make-gofer-address
					   :host host
					   :port port
					   :type type
					   :selector selector))
			   host port))

(defun gofer--directory-url (type display selector &optional host port)
  "Insert a URL (usually HTML) directory entry."
  (let ((url (replace-regexp-in-string "^url:" ""  selector nil)))
    (gofer--directory-link-1 type display selector
			     'url-target url
			     host port)))

(defun gofer--directory-info (type display &rest args)
  "Insert an info directory entry."
  (insert (gofer--leader "") display "\n"))

(defun gofer--directory-terminal (&rest args)
  "Insert a terminal directory entry."
  (insert "."))

(defun gofer--directory-unknown (&rest args)
  "Insert an unknown directory entry."
  (insert (gofer--leader "????") (propertize (format "%S" args) 'face 'bold) "\n"))

(defun gofer--get-directory-processor (type)
  "Find the processor for a directory entry of TYPE."
  (let ((h (gofer--get-handler type)))
    (or (and h (gofer-handler-directory-processor h))
	'gofer--directory-unknown)))

(defun gofer--directory-filter (input)
  "Process a single LINE of a directory document."
  (unless (<= (length input) 1)
    (let* ((line (substring input 0 -1)) ; Get rid of trailing .

	   (components (split-string line "\t"))
	   (type-display (car components))
	   (type (substring type-display 0 1))
	   (display (substring type-display 1))

	   (processor (gofer--get-directory-processor type)))

      (apply processor type display (cdr components)))))

(defun gofer--file-filter (line)
  "Process a single LINE of a text document."
  (unless (<= (length line) 1)
    (insert
     ;; Get rid of the trailing ^M.
     (substring line 0 -1)))
  (insert "\n"))

;;

(defun gofer--line-filter (process string)
  "Deal with STRING from PROCESS in a line-based document."
  (let ((b (process-buffer process)))
    (when (buffer-live-p b)
      (with-current-buffer b
	(let* ((h (gofer--get-handler (gofer-address-type gofer--current-address)))
	       (f (gofer-handler-line-filter h)))
	  (save-excursion
	    (goto-char (process-mark process))
	    (setq gofer--pending-input (concat gofer--pending-input string))
	    (let ((inhibit-read-only t))
	      (mapcar (lambda (line)
			;; If the line has the correct terminator, pass
			;; it on to the processor...
			(if (and (>= (length line) 1)
				 (string= (substring line -1) ""))
			    (funcall f line)
			  ;; ...otherwise store it for next time.
			  (setq gofer--pending-input line)))
		      (split-string gofer--pending-input "\n")))
	    (set-marker (process-mark process) (point))))))))

(defun gofer--accumulator-image ()
  "Yow!"
  (let ((inhibit-read-only t))
    (insert-image (create-image gofer--pending-input nil t))))

(defun gofer--accumulator-filter (process string)
  "Deal with STRING from PROCESS by accumulating it in a variable."
  (let ((b (process-buffer process)))
    (when (buffer-live-p b)
      (with-current-buffer b
	(setq gofer--pending-input (concat gofer--pending-input string))))))

(defun gofer--get-handler (type)
  "Return the handler for content TYPE."
  (cdr (assoc type gofer--handlers)))

(defun gofer--default-saver (filename)
  "Save the contents of the current page to a file."
  (write-region (point-min) (point-max) filename))

(defun gofer--accumulator-saver (filename)
  "Save the contents of the current page to a file."
  (with-temp-buffer
    (insert gofer--pending-input)
    (gofer--default-saver filename)))

(defun gofer--get-saver (handler)
  "Return a function to save pages for this handler."
  (or (gofer-handler-saver handler)
      #'gofer--default-saver))

(defun gofer--parse-address (address)
  "Parse a gopher URL into constiuent parts and return a
`gofer-address'."

  (unless (string-match "^gopher://" address)
    (error "Invalid address (missing protocol): %s" address))

  (let* ((addr (make-gofer-address))

	 (address-body (replace-regexp-in-string "^gopher://" "" address))

	 (components (split-string address-body "/"))

	 (hostname-port (split-string (car components) ":"))
	 (hostname (car hostname-port))
	 (port (cadr hostname-port))

	 (type-selector (mapconcat #'identity (cdr components) "/")))

    (setf (gofer-address-host addr) hostname
	  (gofer-address-port addr) port)

    (if (string= "" type-selector)
	(setf (gofer-address-type addr) "1" ;; Directory.
	      (gofer-address-selector addr) "")
      (setf (gofer-address-type addr) (substring type-selector 0 1)
	    (gofer-address-selector addr) (substring type-selector 1)))

    ;; Addresses must have at least a hostname:
    (let ((h (gofer-address-host addr)))
      (unless (and h
		   (not (string= "" h)))
	(error "Invalid address (missing hostname): %s" address)))

    addr))

(defun gofer--format-address (address)
  "Return ADDRESS as a string."
  (assert (gofer-address-p address))

  (let ((port (gofer-address-port address)))

    (format "gopher://%s%s/%s%s"
	    (gofer-address-host address)
	    (if port
		(concat ":" port)
	      "")
	    (gofer-address-type address)
	    (gofer-address-selector address))))

(defun gofer--network-sentinel (process event)
  (when (string= event "connection broken by remote peer\n")
    (let ((b (process-buffer process)))
      (when (buffer-live-p b)
	(with-current-buffer b
	  (let* ((h (gofer--get-handler (gofer-address-type gofer--current-address)))
		 (a (gofer-handler-accumulator-processor h)))
	    (when a
	      (funcall a)))
	  ;; Restore point when travelling through the history.
	  (when gofer--target-place
	    (goto-char gofer--target-place)))))))

(defun gofer--format-query (address &optional search)
  "Return the gopher query from ADDRESS."
  (assert (gofer-address-p address))

  (format "%s%s\n" (gofer-address-selector address)
	  (if search
	      (concat "\t" search)
	    "")))

(defun gofer--load-address (address buffer)
  "Load the contents of ADDRESS into BUFFER."
  (assert (gofer-address-p address))
  (assert (buffer-live-p buffer))

  (with-current-buffer buffer
    (setq buffer-read-only nil)
    (remove-overlays)
    (erase-buffer)
    (setq gofer--current-address address
	  gofer--pending-input "")
    (let* ((h (gofer--get-handler (gofer-address-type address)))
	   (p (make-network-process :name "*gofer-process*"
				    :buffer buffer
				    :host (gofer-address-host address)
				    :service (or (gofer-address-port address)
						 "gopher")
				    :filter (gofer-handler-filter h)
				    :sentinel 'gofer--network-sentinel
				    :coding (or (gofer-handler-coding h)
						'utf-8-unix)))
	   (search (when (gofer-handler-search h)
		     (read-from-minibuffer "Search: "))))

      (process-send-string p (gofer--format-query address search))

      (setq gofer--current-process p))))

(defun gofer (&optional address place)
  "Browse the gopher page at ADDRESS."
  (interactive "MAddress: ")

  (let ((addr (gofer--parse-address address))
	(buffer (get-buffer-create gofer--buffer-name)))
    (switch-to-buffer buffer)
    (gofer--load-address addr buffer)
    (setq gofer--target-place (or place (point-min)))
    (goto-char (point-min))
    (gofer-mode)
    (setq header-line-format address)))

(defun gofer-edit ()
  "Edit the current address."
  (interactive)
  (gofer
   (read-from-minibuffer "Address: "
			 (gofer--format-address gofer--current-address))))

(defun gofer-refresh ()
  "Refresh the current gopher page."
  (interactive)
  (gofer (gofer--format-address gofer--current-address)))

(defun gofer-quit ()
  "Close the gopher buffer."
  (interactive)
  (kill-buffer (current-buffer)))

(defun gofer-follow ()
  "Follow the link on the current line."
  (interactive)

  (let ((address (get-text-property (point) 'gofer-target)))
    (when address
      (gofer--history-push
       (make-gofer-history :address (gofer--format-address gofer--current-address)
			   :place (point)))
      (gofer address)))

  (let ((url (get-text-property (point) 'url-target)))
    (when url
      (browse-url url))))

(defun gofer-save (&optional filename)
  "Save the current page to a file."
  (interactive "FSave as: ")
  (let* ((h (gofer--get-handler (gofer-address-type gofer--current-address)))
	 (s (gofer--get-saver h)))
    (funcall s filename)))

;;

(defstruct gofer-history
  address place)

(defvar gofer--history nil
  "Browsing history.")
(make-local-variable 'gofer--history)

(defun gofer--history-top ()
  "Return the top of the history stack."
  (car gofer--history))

(defun gofer--history-push (entry)
  "Push ENTRY onto the history stack."
  (assert (gofer-history-p entry))

  ;; Don't push duplicates.
  (let ((top (gofer--history-top)))
    (unless (eq entry top)
      (push entry gofer--history))))

(defun gofer--history-pop ()
  "Return the top of the history stack and remove it from the stack."
  (pop gofer--history))

(defun gofer-history-back ()
  "Step back through the gofer history."
  (interactive)

  (let ((entry (gofer--history-pop)))
    (if entry
	(gofer (gofer-history-address entry)
	       (gofer-history-place entry))
      (error "No more history."))))

;;

(defvar gofer-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map "e" 'gofer-edit)
    (define-key map "g" 'gofer)
    (define-key map "s" 'gofer-save)
    (define-key map "q" 'gofer-quit)
    (define-key map "=" 'gofer-refresh)
    (define-key map (kbd "RET") 'gofer-follow)
    (define-key map (kbd "DEL") 'scroll-down)
    (define-key map " " 'scroll-up)
    (define-key map "b" 'gofer-history-back)
    map)
  "Keymap for gofer buffers.")
(fset 'gofer-mode-map gofer-mode-map)

(define-derived-mode gofer-mode fundamental-mode "gofer"
  "Major mode for examining Gopher documents."
  (setq buffer-read-only t))

;;

(provide 'gofer)

;;; gofer.el ends here
